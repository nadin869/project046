package com.luxoft.sqa;

import org.testng.Assert;
import org.testng.annotations.Test;

public class DistanceTest {

    @Test
    public void distanceEqualPoints(){
        Point First = new Point(1,1);
        Point Second = new Point(1,1);
        Assert.assertEquals(First.distance(Second), 0.0);
    }

    @Test
    public void distanceEqualX(){
        Point First = new Point(1,4);
        Point Second = new Point(1,1);
        Assert.assertEquals(First.distance(Second), 3.0);
    }

    @Test
    public void distanceEqualY(){
        Point First = new Point(4,1);
        Point Second = new Point(1,1);
        Assert.assertEquals(First.distance(Second), 3.0);
    }

    @Test
    public void distanceRandomPoints(){
        Point First = new Point(0,0);
        Point Second = new Point(3,4);
        Assert.assertEquals(First.distance(Second), 5.0);
    }

    @Test
    public void distanceThemSelf(){
        Point First = new Point(0,0);
        Assert.assertEquals(First.distance(First), 0.0);
    }
}
