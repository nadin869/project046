package com.luxoft.sqa;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SquareTest {
    @Test
    public void testAreaSquare(){
        Square s = new Square(5);
        assert s.sayArea() == 25;
    }

    @Test
    public void testAreaRectangle(){
        Rectangle r = new Rectangle(2,2);
        Assert.assertEquals(r.sayArea(),4.0);
    }
}
