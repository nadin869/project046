package com.luxoft.sqa;


public class Square {
    private double length;

    public Square(double length){
        this.length = length;
    }

    public double sayArea(){
        return length*length;
    }

}
