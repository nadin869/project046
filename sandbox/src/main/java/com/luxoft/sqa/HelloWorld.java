package com.luxoft.sqa;

import java.util.Date;

public class HelloWorld{
	public static void main(String[] args){

		Point First = new Point(2, 6);
		Point Second = new Point(1, 5);
		System.out.println(First.distance(Second));
		Square s = new Square(2.3);
		Rectangle r = new Rectangle(2, 5);

		System.out.println(s.sayArea());
		System.out.println(r.sayArea());

		Date d  = new Date();
		System.out.println(d);
	}
}