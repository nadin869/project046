package com.luxoft.sqa;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Point {
    private int x;
    private int y;

    public Point (int x, int y){
        this.x = x;
        this.y = y;
    }

    public double distance (Point a){
        double d;
        d = sqrt(pow((a.x-x),2)+pow((a.y - y),2));
        return d;
    }
}
