package com.luxoft.sqa.webtest;

import com.luxoft.sqa.model.ContactData;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Comparator;
import java.util.List;

public class CreateNewContactTest extends TestBase{
    @Test(enabled = false)
    protected void testAddNewCreation() {
        application.goTO().goToAddNew();
        List<ContactData> before = application.getContactHelper().getContactList();
        ContactData contact = new ContactData("Ctest", "new3", "new4", "emaail@mail.ru");
        application.getContactHelper().createContact(contact);
        // application.getContactHelper().selectFirstContact();
        List<ContactData> after = application.getContactHelper().getContactList();
        Comparator<? super ContactData> byId = (g1, g2) -> Integer.compare(g1.getId(), g2.getId());
        before.add(contact);
        before.sort(byId);
        after.sort(byId);
        Assert.assertEquals(before, after);
    }

}