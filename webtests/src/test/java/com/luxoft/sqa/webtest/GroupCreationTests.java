package com.luxoft.sqa.webtest;

import com.luxoft.sqa.model.GroupData;
import com.luxoft.sqa.model.Groups;

import org.testng.annotations.Test;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

public class GroupCreationTests extends TestBase {
    @Test
    public void testGroupCreation() {
        application.goTO().goToGroupPage();
       // Set<GroupData> before = application.getGroupHelper().all();
        Groups before = application.getGroupHelper().allGroups();
        GroupData group = new GroupData().withName("name").withHeader("header").withFooter("footer");
       application.getGroupHelper().createGroup(group);
      //  Set<GroupData> after = application.getGroupHelper().all();
        Groups after = application.getGroupHelper().allGroups();
        assertThat(after, equalTo(before.withAdded(group.withId(after.stream().mapToInt((g) -> g.getId()).max().getAsInt()))));




//        group.withId(after.stream().mapToInt((g) -> g.getId()).max().getAsInt());
//        before.add(group);
//        Assert.assertEquals(before, after);
    }
}
