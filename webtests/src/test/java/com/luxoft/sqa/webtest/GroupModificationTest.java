package com.luxoft.sqa.webtest;

import com.luxoft.sqa.model.GroupData;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Comparator;
import java.util.List;
import java.util.Set;

public class GroupModificationTest extends TestBase {


    @BeforeMethod
    public void ensurePrecondition(){
        application.goTO().goToGroupPage();
        if( application.getGroupHelper().isThereAGroup()){
            application.getGroupHelper().createGroup(new GroupData()
                    .withName("test1")
                    .withHeader("test2")
                    .withFooter("test3"));
        }
    }

    @Test
    public void testGroupModification(){

        Set<GroupData> before = application.getGroupHelper().all();
        GroupData modifiedGroup = before.iterator().next();
        GroupData group  = new GroupData().withId(modifiedGroup.getId()).withName("new-modify").withHeader("new22-modify").withFooter("new3-modify");
        application.getGroupHelper().modifyGroup(group);
        Set<GroupData> after = application.getGroupHelper().all();
        before.remove(modifiedGroup);
        before.add(group);
        Assert.assertEquals(before, after);
    }

}
