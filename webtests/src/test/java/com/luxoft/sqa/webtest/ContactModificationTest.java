package com.luxoft.sqa.webtest;

import com.luxoft.sqa.model.ContactData;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Comparator;
import java.util.List;

public class ContactModificationTest extends TestBase{
    @Test(enabled = false)
    protected  void testContactModification(){
         if(! application.getContactHelper().isThereAContact()){
            application.getContactHelper().createContact(new ContactData("test", "test2", null, null));
        }
        int index = application.getContactHelper().getContactCount();
        List<ContactData> before = application.getContactHelper().getContactList();
        application.getContactHelper().selectContact(index -1);
        application.getContactHelper().initContactModification();
        ContactData contact  = new ContactData(before.get(index-1).getId(),"new78", "new25", "new35", "new@mail.ru");
        application.getContactHelper().fillFieldsOnNewAddressBookEntry(contact);
        application.getContactHelper().updateContactModification();
        application.getContactHelper().returnToHomePage();
        List<ContactData> after = application.getContactHelper().getContactList();
        before.remove(index -1);
        before.add(contact);
            ////Addition of sorting -----------
        Comparator<? super ContactData> byId = (g1, g2) -> Integer.compare(g1.getId(), g2.getId());
        before.sort(byId);
        after.sort(byId);
        Assert.assertEquals(before, after);
    }

}
