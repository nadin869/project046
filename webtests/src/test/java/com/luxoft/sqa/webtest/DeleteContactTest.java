package com.luxoft.sqa.webtest;

import com.luxoft.sqa.model.ContactData;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class DeleteContactTest extends TestBase {
    @Test(enabled = false)
    protected void testDeleteContact() {
        if(!application.getContactHelper().isThereAContact()){
            application.getContactHelper().createContact(new ContactData("new1","new2", "new3", "new4@mail.ru"));
        }
        List<ContactData> before = application.getContactHelper().getContactList();
        int index = application.getContactHelper().getContactCount();
        application.getContactHelper().selectContact(index -1);
        application.getContactHelper().deleteContact();
        application.getContactHelper().returnToHomePage();
        List<ContactData> after = application.getContactHelper().getContactList();
        before.remove(before.size()-1);
        Assert.assertEquals(before,after);
    }
}
