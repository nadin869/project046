package com.luxoft.sqa.webtest;

import com.luxoft.sqa.model.GroupData;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Set;

public class GroupDeletionTest extends TestBase {

    @BeforeMethod
    public void ensurePrecondition(){
        application.goTO().goToGroupPage();
        if( application.getGroupHelper().isThereAGroup()){
            application.getGroupHelper().createGroup(new GroupData()
                    .withName("test1")
                    .withHeader("test2")
                    .withFooter("test3"));
        }
    }
    @Test
    public void testGroupDeletion() {
        Set<GroupData> before = application.getGroupHelper().all();
        GroupData deletedGroup = before.iterator().next();
        application.getGroupHelper().deletedGroup(deletedGroup);
        Set<GroupData> after = application.getGroupHelper().all();
        before.remove(deletedGroup);
        Assert.assertEquals(before, after);
    }
}

