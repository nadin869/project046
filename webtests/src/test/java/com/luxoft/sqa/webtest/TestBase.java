package com.luxoft.sqa.webtest;


import com.luxoft.sqa.framework.Application;
import org.openqa.selenium.remote.BrowserType;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;


public class TestBase {
    public static Application application = new Application(BrowserType.CHROME);

   // @BeforeMethod
   @BeforeSuite
    public void setUp() {
        application.init();

    }
   // @AfterMethod
    @AfterSuite
    public void tearDown() {
        application.stop();
    }
}
