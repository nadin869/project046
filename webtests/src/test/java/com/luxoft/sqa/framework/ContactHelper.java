package com.luxoft.sqa.framework;

import com.luxoft.sqa.model.ContactData;
import com.luxoft.sqa.model.GroupData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;


public class ContactHelper extends BaseHelper {
    public ContactHelper(WebDriver driver) {
        super(driver);
    }

    public void returnToHomePage() {
        click(By.linkText("home"));
    }

    public void fillFieldsOnNewAddressBookEntry(ContactData contactData) {
        type(By.name("firstname"), contactData.getFirstName());
        type(By.name("lastname"), contactData.getLastName());
        type(By.name("mobile"), contactData.getMobile());
        type(By.name("email"), contactData.getEmail());
//                if(creation){
//            new Select(driver.findElement(By.name("new_group"))).selectByIndex(0);
//        }else {
//            Assert.assertFalse(isElementPresent(By.name("new_group")));
//        }

        }

    public void initContactCreation() {
        click(By.linkText("add new"));
    }

    //public void addNewContact() {click(By.xpath("//*[@id=\"content\"]/form/input[21]"));    }
//    public void selectFirstContact() {
//
//        click(By.name("selected[]"));
//    }

    public void selectContact(int index) {
        driver.findElements(By.name("selected[]")).get(index).click();
    }

    public void deleteContact() {

        click(By.cssSelector("input[value=\"Delete\"]"));
        driver.switchTo().alert().accept();
    }

    public void initContactModification() {
        click(By.cssSelector("img[alt=\"Edit\"]"));
    }
    public void updateContactModification() {
        click(By.cssSelector("input[type=\"submit\"]"));
    }

    public boolean isThereAContact() {
        return  isElementPresent(By.name("selected[]"));
    }
    public void submitContactCreation() {
        click(By.name("submit"));
    }

    public void createContact(ContactData contactData) {

        fillFieldsOnNewAddressBookEntry(contactData);
        submitContactCreation();
        returnToHomePage();
    }

    public int getContactCount() {
        return driver.findElements(By.name("selected[]")).size();
    }

    public List<ContactData> getContactList() {

        List<ContactData> contacts = new ArrayList<ContactData>();
        List<WebElement> elements = driver.findElements(By.cssSelector("#maintable > tbody > tr:nth-child(2)"));
        for (WebElement element: elements) {
            String lastName = element.getText();
            //int id = Integer.parseInt(element.findElement(By.name("selected[]")).getAttribute("value"));
            int id = Integer.parseInt(element.findElement(By.tagName("input")).getAttribute("value"));
            ContactData contact = new ContactData(id, null, lastName, null, null);
            contacts.add(contact);
        }
        return contacts;
    }
}
